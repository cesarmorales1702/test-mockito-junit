package com.tower.crtn.test.mockito.junit.exception;

import com.tower.crtn.test.mockito.junit.exception.model.BusinessException;
import com.tower.crtn.test.mockito.junit.exception.model.ErrorResponse;
import com.tower.crtn.test.mockito.junit.exception.model.ErrorType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ErrorResolver {

  private ErrorResponse getErrorResponse(ErrorType type, BusinessException ex) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setType(type.name());
    errorResponse.setCode(ex.getCode());
    errorResponse.setDetails(ex.getDetails());
    return errorResponse;
  }

  private ErrorResponse getErrorResponse(ErrorType type, Exception ex) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setType(type.name());
    errorResponse.setCode("500");
    errorResponse.setDetails(ex.getMessage());
    return errorResponse;
  }

  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
  public ErrorResponse resolveBusinessException(HttpServletRequest req, BusinessException ex) {
    return getErrorResponse(ErrorType.ERROR, ex);
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse resolveAnyError(HttpServletRequest req, Exception ex) {
    return getErrorResponse(ErrorType.FATAL, ex);
  }

}
